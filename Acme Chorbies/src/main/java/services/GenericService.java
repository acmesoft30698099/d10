package services;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import services.utils.ServicesUtils;
import domain.DomainEntity;

/**
 * Clase gen�rica de servicios, si es necesario modificar uno de los servicios por reglas de negocio,
 * usar @Override. 
 * @author Student
 *
 * @param <T> Repositorio
 * @param <G> Objeto del dominio
 */
public abstract class GenericService<T extends JpaRepository<G, Integer>, G extends DomainEntity> {
	
	// Constructor ------------------------
	
	public GenericService() {
		super();
	}
	
	// Repository -------------------------
	
	@Autowired
	protected T repository;
	
	// Service ----------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	// CRUD Methods -----------------------------
	
	public G findOne(Integer id) {
		servicesUtils.checkId(id);
		return repository.findOne(id);
	}
	
	public G findOneForEdit(Integer id){
		servicesUtils.checkId(id);
		G obj = findOne(id);
		obj = reglaNegocioGeneral(obj);
		obj = reglaNegocioEdit(obj);
		return obj;
	}
	
	public Collection<G> findAll() {
		return repository.findAll();
	}
	
	public Collection<G> findAll(Iterable<Integer> ids) {
		servicesUtils.checkIterable(ids);
		return repository.findAll(ids);
	}
	
	public G save(G obj) {
		G saved;
		obj = reglaNegocioGeneral(obj);
		obj = reglaNegocioSave(obj);
		saved = repository.save(obj);
		return saved;
	}
	
	public void delete(G obj) {
		reglaNegocioDelete(obj);
		repository.delete(obj.getId());
	}
	
	public G reglaNegocioGeneral(G obj){
		return obj;
	}
	
	public G reglaNegocioSave(G obj){
		return obj;
	}
	
	public G reglaNegocioDelete(G obj){
		return obj;
	}
	
	public G reglaNegocioEdit(G obj){
		return obj;
	}
	
	public void flush() {
		repository.flush();
	}

}
