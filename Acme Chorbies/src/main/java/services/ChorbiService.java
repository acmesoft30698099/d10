package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ChorbiRepository;
import security.Authority;
import security.UserAccount;
import security.UserAccountService;
import services.utils.ServicesUtils;
import domain.Chirp;
import domain.Chorbi;
import domain.Likes;
import forms.ChorbiForm;
import forms.UserAccountForm;

@Service
@Transactional
public class ChorbiService extends GenericService<ChorbiRepository, Chorbi> {

	// Managed repository -----------------------------------------------------
	
	@Autowired
	private ChorbiRepository chorbiRepository;
	
	// Supporting services ----------------------------------------------------

	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	@Autowired
	private UserAccountService userAccountService;
	
	// Constructors -----------------------------------------------------------

	public ChorbiService() {
		super();
	}

	// Simple CRUD methods ----------------------------------------------------

	public Chorbi create() {
		Chorbi created = new Chorbi();
		created.setReceivedChirps(new ArrayList<Chirp>());
		created.setReceivedLikes(new ArrayList<Likes>());
		created.setSentChirps(new ArrayList<Chirp>());
		created.setSentLikes(new ArrayList<Likes>());
		created.setUserAccount(new UserAccount());
		return created;
	}
	
	@Override
	public Chorbi reglaNegocioSave(Chorbi chorbi) {
		Assert.notNull(chorbi);
		// Guardamos la UserAccount si se trata de un registro de nuevo usuario
		if (chorbi.getId() <= 0) {
			Assert.notNull(chorbi.getUserAccount());
			UserAccount ua = userAccountService.save(chorbi.getUserAccount());
			chorbi.setUserAccount(ua);
		}
		Assert.isTrue(chorbi.getAge()>=18,"BussinessRule.IncorrectAge");
		return chorbi;
	}
	
	@Override
	public Chorbi reglaNegocioDelete(Chorbi chorbi) {
		Chorbi registered;
		Assert.notNull(chorbi);
		Assert.isTrue(chorbi.getId() > 0);
		// Obtenemos el original y comprobamos que el usuario registrado sea el
		// propietario de la cuenta
		chorbi = chorbiRepository.findOne(chorbi.getId());
		Assert.notNull(chorbi);
		registered = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
		Assert.notNull(registered);
		Assert.isTrue(registered.getId() == chorbi.getId());
		return registered;
	
	}
	
	@Override
	public Chorbi reglaNegocioEdit(Chorbi chorbi){
		Assert.notNull(chorbi);
		Chorbi registered = servicesUtils.checkRegistered(Chorbi.class, servicesUtils.checkUser());
		Assert.notNull(registered);
		Assert.isTrue(registered.getId() == chorbi.getId());
		return chorbi;
	}
	
	// Other methods --------------------------------
	
	public Chorbi reconstruct(ChorbiForm actorForm, BindingResult binding) {
		Chorbi newChorbi = create();
		if (actorForm.getId() > 0) {
			Chorbi original = findOneForEdit(actorForm.getId());
			
			newChorbi.setId(original.getId());
			newChorbi.setVersion(original.getVersion());
			
			newChorbi.setFinder(original.getFinder());
			newChorbi.setSentChirps(original.getReceivedChirps());
			newChorbi.setReceivedChirps(original.getReceivedChirps());
			newChorbi.setSentLikes(original.getReceivedLikes());
			newChorbi.setReceivedLikes(original.getReceivedLikes());
			newChorbi.setUserAccount(original.getUserAccount());
		}
		else {
			UserAccount ua = userAccountService.create();
			Collection<Authority> authorities = new ArrayList<Authority>();
			Authority authority = new Authority();
			authority.setAuthority(Authority.CHORBI);
			authorities.add(authority);
			ua.setAuthorities(authorities);
			
			ua.setUsername(actorForm.getUserAccount().getUsername());
			ua.setPassword(actorForm.getUserAccount().getPassword());
			newChorbi.setUserAccount(ua);
		}

		newChorbi.setName(actorForm.getName());
		newChorbi.setSurname(actorForm.getSurname());
		newChorbi.setPhone(actorForm.getPhone());
		newChorbi.setEmail(actorForm.getEmail());
		newChorbi.setImage(actorForm.getImage());
		newChorbi.setRelation(actorForm.getRelation());
		newChorbi.setDescription(actorForm.getDescription());
		newChorbi.setGender(actorForm.getGender());
		newChorbi.setBirthDate(actorForm.getBirthDate());
		newChorbi.setCoordinate(actorForm.getCoordinate());

		validator.validate(newChorbi, binding);

		return newChorbi;
	}
	
	public ChorbiForm construct(Chorbi chorbi) {
		ChorbiForm actorForm = new ChorbiForm();

		actorForm.setId(chorbi.getId());

		actorForm.setName(chorbi.getName());
		actorForm.setSurname(chorbi.getSurname());
		actorForm.setPhone(chorbi.getPhone());
		actorForm.setEmail(chorbi.getEmail());
		actorForm.setImage(chorbi.getImage());
		actorForm.setRelation(chorbi.getRelation());
		actorForm.setDescription(chorbi.getDescription());
		actorForm.setGender(chorbi.getGender());
		actorForm.setBirthDate(chorbi.getBirthDate());
		actorForm.setCoordinate(chorbi.getCoordinate());
		
		actorForm.setUserAccount(new UserAccountForm());

		return actorForm;
	}
	
}