package services.utils;

import java.util.Iterator;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;

import security.LoginService;
import security.UserAccount;
import services.ActorService;
import domain.Actor;

/**
 * Clase de utilidades de los servicios
 * @author Ignacio Jos� Al�s L�pez
 */
@Service
@Transactional
public class ServicesUtils {
	
	// Services
	
	@Autowired
	private ActorService actorService;
	
	// Methods
	
	/**
	 * Compruba que el Iterable aportado contiene ids correctos.
	 * @param ids El Iterable a comprobar
	 * @throws IllegalArgumentException Si se encuentra alg�n id incorrecto
	 */
	public void checkIterable(Iterable<Integer> ids) {
		Assert.notNull(ids);
		Iterator<Integer> iter = ids.iterator();
		Assert.isTrue(iter.hasNext());
		while(iter.hasNext()) {
			Integer id = iter.next();
			checkId(id);
		}
	}
	/**
	 * Comprueba que el Integer aportado es un id correcto.
	 * @param id El id a comprobar.
	 * @throws IllegalArgumentException Si el id es incorrecto.
	 */
	public void checkId(Integer id) {
		Assert.notNull(id);
		Assert.isTrue(id > 0);
	}
	
	/**
	 * Comprueba que el usuario registrado tenga cuenta en el sistema
	 * @throws IllegalArgumentException Cuando el usuario no est� registrado.
	 */
	public Actor checkUser() {
		UserAccount user = LoginService.getPrincipal();
		// Como getPrincipal lanza excepci�n si no se encuentra el objeto, no es necesario tratarlo.
		Actor a = actorService.findActorByUserAccount(user);
		Assert.notNull(a);
		return a;
	}
	/**
	 * Retorna el actor convertido a la clase aportada. Lanzar� excepci�n si el actor no es de ese tipo.
	 * @param classObj La clase del objecto esperado
	 * @param a El actor a convertir
	 * @return El actor convertido
	 * @throws IllegalArgumentException Si el objeto no se puede convertir a la clase.
	 */
	public <T extends Actor> T checkRegistered(Class<T> classObj, Actor a) {
		Assert.notNull(a);
		Assert.isTrue(a.getClass().isAssignableFrom(classObj));
		@SuppressWarnings("unchecked")
		T result = (T) a;
		return result;
	}
	
	public <T extends Actor> T getRegisteredUser(Class<T> classObj) {
		Actor a = checkUser();
		Assert.notNull(a);
		return checkRegistered(classObj, a);
	}
	
	public String encryptPassword(String password) {
		Md5PasswordEncoder md5PasswordEncoder = new Md5PasswordEncoder();
		return md5PasswordEncoder.encodePassword(password, null);
	}
	
	/**
	 * Comprueba que el UserAccount de entrada sea correcto.
	 * @param ua El UserAccount a comprobar.
	 * @param binding El binding del formulario.
	 */
	public void validarUserAccount(UserAccount ua, BindingResult binding) {
		if (ua.getUsername().isEmpty())
			binding.rejectValue("userAccount.username",
					"org.hibernate.validator.constraints.NotBlank.message");
		if (ua.getPassword().isEmpty())
			binding.rejectValue("userAccount.password",
					"org.hibernate.validator.constraints.NotBlank.message");
	}

}
