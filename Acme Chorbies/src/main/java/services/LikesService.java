package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.LikesRepository;
import services.utils.ServicesUtils;
import domain.Chorbi;
import domain.Likes;
import forms.LikesForm;

@Service
@Transactional
public class LikesService extends GenericService<LikesRepository, Likes> {
	
	// Constructor -----------------------------------
	
	public LikesService() {
		super();
	}
	
	// Service ---------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	// CRUD methdos ----------------------------------
	
	public Likes create() {
		Likes likes = new Likes();
		
		return likes;
	}
	
	public Likes reglaNegocioEdit(Likes likes) {
		// Comprobamos los permisos, tanto el creador como receptor podr�n editar los Likes, que 
		// se reduce a poder eliminarlos
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(likes.getSender().getId() == chorbi.getId()
				|| likes.getReceiver().getId() == chorbi.getId());
		
		return likes;
	}
	
	public Likes reglaNegocioSave(Likes likes) {
		// Comprobamos los permisos
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(likes.getSender().getId() == chorbi.getId());
		// Comprobamos que no est� realizando like a si mismo
		Assert.isTrue(likes.getSender().getId() != likes.getReceiver().getId());
		// Actualizamos la fecha de envio
		likes.setSentDate(new Date());
		
		return likes;
	}
	
	public Likes reglaNegocioDelete(Likes likes) {
		// Comprobamos los permisos, tanto el creador como receptor podr�n eliminarlos
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		Assert.isTrue(likes.getSender().getId() == chorbi.getId()
				|| likes.getReceiver().getId() == chorbi.getId());
		
		return likes;
	}

	// Other methods -----------------------------------
	
	public LikesForm construct(Likes likes) {
		LikesForm likesForm = new LikesForm();
		
		likesForm.setComments(likes.getComments());
		likesForm.setReceiver(likes.getReceiver());
		
		return likesForm;
	}
	
	public Likes reconstruct(LikesForm likesForm, BindingResult binding) {
		Likes likes = create();
		
		// S�lo se admite la creaci�n, por lo tanto siempre se definir�n los datos no mutables
		Chorbi chorbi = servicesUtils.getRegisteredUser(Chorbi.class);
		likes.setSender(chorbi);
		likes.setSentDate(new Date());
		
		likes.setComments(likesForm.getComments());
		likes.setReceiver(likesForm.getReceiver());
		
		validator.validate(likes, binding);
		
		return likes;
	}
	
}
