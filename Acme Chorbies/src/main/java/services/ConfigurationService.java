package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import repositories.ConfigurationRepository;
import services.utils.ServicesUtils;
import domain.Administrator;
import domain.Configuration;
import forms.ConfigurationForm;

@Service
@Transactional
public class ConfigurationService extends GenericService<ConfigurationRepository, Configuration> {
	
	// Constructor ----------------------------------
	
	public ConfigurationService() {
		super();
	}
	
	// Services -------------------------------------
	
	@Autowired
	private ServicesUtils servicesUtils;
	
	@Autowired
	private Validator validator;
	
	// CRUD Methods ---------------------------------
	
	public Configuration reglaNegocioEdit(Configuration configuration) {
		// Comprobamos que el usuario actual sea Administrator
		servicesUtils.getRegisteredUser(Administrator.class);
		
		return configuration;
	}
	
	public Configuration reglaNegocioSave(Configuration configuration) {
		// Misma comprobaci�n que el Edit, comprobamos que el usuario sigue siendo Administrator
		servicesUtils.getRegisteredUser(Administrator.class);
		
		return configuration;
	}
	
	public Configuration reglaNegocioDelete(Configuration configuration) {
		// Nunca se podr� eliminar la Configuration, para evitarlo, ponemos esta restricci�n
		Assert.isTrue(false);
		
		return null;
	}
	
	// Other methods --------------------------------
	
	/**
	 * Retorna el Configuration de la aplicaci�n.
	 * @return
	 */
	public Configuration findConfiguration() {
		Collection<Configuration> configurationCollection = repository.findAll();
		return configurationCollection.iterator().next();
	}
	
	public ConfigurationForm construct(Configuration configuration) {
		ConfigurationForm confForm = new ConfigurationForm();
		
		confForm.setBanners(configuration.getBanners());
		confForm.setTimer(configuration.getTimer());
		
		return confForm;
	}
	
	public Configuration reconstruct(ConfigurationForm confForm, BindingResult binding) {
		Configuration result;
		
		// Siempre se editar� el objeto, nunca creando, entonces lo obtenemos de la BD
		result = findConfiguration();
		
		result.setBanners(confForm.getBanners());
		result.setTimer(confForm.getTimer());
		
		validator.validate(result,binding);
		
		return result;
	}
}
