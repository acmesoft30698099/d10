package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;

@Repository
public interface ChorbiRepository extends JpaRepository<Chorbi, Integer>{
	
	@Query("select concat(coordinate.country,', ',coordinate.city), count(chorbi) from Chorbi chorbi join chorbi.coordinate group by coordinate.country, coordinate.city")
	Object[][] getNumberOfChorbiesByTheirCountryAndCity();
	
	@Query("select max(chorbi.age) from Chorbi chorbi")
	Integer getMaxAgeOfChorbies();
	
	@Query("select min(chorbi.age) from Chorbi chorbi")
	Integer getMinAgeOfChorbies();
	
	@Query("select avg(chorbi.age) from Chorbi chorbi")
	Double getAvgAgeOfChorbies();
	
	@Query("select count(chorbi) from Chorbi chorbi where chorbi.creditCard = null or chorbi.creditCard.valid = false")
	Long getAmountOfChorbieswithInvalidOrNullCreditCard();
	
	@Query("select count(finder) from Finder finder where finder.relation like ?1")
	Long getAmountOfChorbiesSearchingFor(String relation);
	
	@Query("select chorbi from Chorbi chorbi order by chorbi.receivedLikes")
	Collection<Chorbi> getChorbiesOrderedByNumberOfReceivedLikes();
	
	@Query("select min(chorbi.receivedLikes.size) from Chorbi chorbi")
	Integer getMinNumberOfLikesPerChorbi();
	
	@Query("select max(chorbi.receivedLikes.size) from Chorbi chorbi")
	Integer getMaxNumberOfLikesPerChorbi();
	
	@Query("select avg(chorbi.receivedLikes.size) from Chorbi chorbi")
	Double getAvgNumberOfLikesPerChorbi();
	
	@Query("select min(chorbi.receivedChirps.size) from Chorbi chorbi")
	Integer getMinNumberOfReceivedChirpsPerChorbi();
	
	@Query("select max(chorbi.receivedChirps.size) from Chorbi chorbi")
	Integer getMaxNumberOfReceivedChirpsPerChorbi();
	
	@Query("select avg(chorbi.receivedChirps.size) from Chorbi chorbi")
	Double getAvgNumberOfReceivedChirpsPerChorbi();
	
	@Query("select min(chorbi.sentChirps.size) from Chorbi chorbi")
	Integer getMinNumberOfSentChirpsPerChorbi();
	
	@Query("select max(chorbi.sentChirps.size) from Chorbi chorbi")
	Integer getMaxNumberOfSentChirpsPerChorbi();
	
	@Query("select avg(chorbi.sentChirps.size) from Chorbi chorbi")
	Double getAvgNumberOfSentChirpsPerChorbi();
	
	@Query("select chorbi from Chorbi chorbi where chorbi.receivedChirps.size = (select max(chorbi.receivedChirps.size) from Chorbi)")
	Chorbi getChorbiesWithTheMostReceivedChirps();
	
	@Query("select chorbi from Chorbi chorbi where chorbi.sentChirps.size = (select max(chorbi.sentChirps.size) from Chorbi)")
	Chorbi getChorbiesWithTheMostSentChirps();
}
