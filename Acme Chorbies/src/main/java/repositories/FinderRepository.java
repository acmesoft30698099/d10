package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Chorbi;
import domain.Finder;

@Repository
public interface FinderRepository extends JpaRepository<Finder, Integer>{
	
	// TODO Probar que funciona con las coordenadas de entrada
	@Query("select chorbi from Chorbi chorbi join chorbi.coordinate coordinate where chorbi.gender like ?1 and chorbi.relation like ?2 " +
			"and chorbi.description like ?3 and (chorbi.age between (?4 - 5) and (?4 + 5))")
	Collection<Chorbi> getResultsOfFinderWithAge(String gender, String relation, String keyword, Integer age);
	
	@Query("select chorbi from Chorbi chorbi join chorbi.coordinate coordinate where chorbi.gender like ?1 and chorbi.relation like ?2 " +
			"and chorbi.description like ?3")
	Collection<Chorbi> getResultsOfFinderWithoutAge(String gender, String relation, String keyword);

}