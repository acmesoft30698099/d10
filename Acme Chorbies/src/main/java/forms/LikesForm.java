package forms;

import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import domain.Chorbi;

public class LikesForm {

	// Constructor -----------------------------------

	public LikesForm() {
		super();
	}

	// Attributes ------------------------------------

	private String comments;

	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	// Relationships ---------------------------------
	
	private Chorbi receiver;
	
	@NotNull
	@Valid
	@ManyToOne
	public Chorbi getReceiver() {
		return receiver;
	}
	public void setReceiver(Chorbi receiver) {
		this.receiver = receiver;
	}

}
