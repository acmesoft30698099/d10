package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Actor;
import repositories.ActorRepository;

@Component
@Transactional
public class StringToActorConverter extends StringToGenericConverter<Actor, ActorRepository> {

}
