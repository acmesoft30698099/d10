package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.LikesRepository;
import domain.Likes;

@Component
@Transactional
public class StringToLikesConverter extends StringToGenericConverter<Likes, LikesRepository>{

}
