package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Chorbi;

@Component
@Transactional
public class ChorbiToStringConverter extends GenericToStringConverter<Chorbi> {

}
