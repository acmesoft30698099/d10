package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.FinderRepository;
import domain.Finder;

@Component
@Transactional
public class StringToFinderConverter extends StringToGenericConverter<Finder, FinderRepository>{

}
