package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import domain.Likes;

@Component
@Transactional
public class LikesToStringConverter extends GenericToStringConverter<Likes> {

}
