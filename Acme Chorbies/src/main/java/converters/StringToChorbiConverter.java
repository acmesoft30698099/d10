package converters;

import javax.transaction.Transactional;

import org.springframework.stereotype.Component;

import repositories.ChorbiRepository;
import domain.Chorbi;

@Component
@Transactional
public class StringToChorbiConverter extends StringToGenericConverter<Chorbi, ChorbiRepository> {

}
