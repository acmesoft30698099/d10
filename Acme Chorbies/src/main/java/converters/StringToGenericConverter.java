package converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.util.StringUtils;

import domain.DomainEntity;

public class StringToGenericConverter<T extends DomainEntity, R extends JpaRepository<T, Integer>> implements Converter<String, T> {
	
	// Constructor ---------------------------------------
	
	public StringToGenericConverter() {
		super();
	}
	
	// Repository ----------------------------------------
	
	@Autowired
	private R repository;
	
	// Converter -----------------------------------------

	@Override
	public T convert(String source) {
		T result;
		int id;
		
		try {
			if(StringUtils.isEmpty(source)) {
				result = null;
			} else {
				id = Integer.valueOf(source);
				result = repository.findOne(id);
			}
		} catch(Throwable excp) {
			throw new IllegalArgumentException();
		}
		
		return result;
	}
	
	

}
