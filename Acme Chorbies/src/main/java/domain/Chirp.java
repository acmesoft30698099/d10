package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Chirp extends DomainEntity {
	
	// Constructor ------------------------------------
	
	public Chirp() {
		super();
	}
	
	// Attributes ------------------------------------
	
	private Date sentDate;
	private String subject;
	private String text;
	private Collection<Url> attachments;
	
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	@NotBlank
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@NotBlank
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	@ElementCollection
	public Collection<Url> getAttachments() {
		return attachments;
	}
	public void setAttachments(Collection<Url> attachments) {
		this.attachments = attachments;
	}
	
	// Relationships -------------------------------------------
	
	private Chorbi sender;
	private Chorbi receiver;

	@NotNull
	@Valid
	@ManyToOne
	public Chorbi getSender() {
		return sender;
	}
	public void setSender(Chorbi sender) {
		this.sender = sender;
	}
	
	@NotNull
	@Valid
	@ManyToOne
	public Chorbi getReceiver() {
		return receiver;
	}
	public void setReceiver(Chorbi receiver) {
		this.receiver = receiver;
	}

}
