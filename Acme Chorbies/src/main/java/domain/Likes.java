package domain;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Likes extends DomainEntity {
	
	// Constructor -----------------------------------
	
	public Likes() {
		super();
	}
	
	// Attributes ------------------------------------
	
	private Date sentDate;
	private String comments;
	
	@DateTimeFormat(pattern="dd/MM/yyyy HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	// Relationships ---------------------------------------
	
	private Chorbi sender;
	private Chorbi receiver;

	@NotNull
	@Valid
	@ManyToOne
	public Chorbi getSender() {
		return sender;
	}
	public void setSender(Chorbi sender) {
		this.sender = sender;
	}
	
	@NotNull
	@Valid
	@ManyToOne
	public Chorbi getReceiver() {
		return receiver;
	}
	public void setReceiver(Chorbi receiver) {
		this.receiver = receiver;
	}
}
