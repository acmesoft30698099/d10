package domain;

import java.beans.Transient;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Chorbi extends Actor {
	
	// Constructor --------------------------------------
	
	public Chorbi() {
		
	}
	
	// Attributes ---------------------------------------
	
	private String image;
	private String relation;
	private String description;
	private String gender;
	private Date birthDate;
	private Coordinate coordinate;
	private int age;
	
	@URL
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	@Pattern(regexp="activities|friendship|love")
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	
	@NotBlank
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Pattern(regexp="man|woman")
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	@NotNull
	@Valid
	public Coordinate getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}

	@Transient
	public int getAge() {
		LocalDate birthDate = LocalDate.fromDateFields(this.getBirthDate());
		LocalDate now = LocalDate.now();
		this.age = Years.yearsBetween(birthDate, now).getYears();
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}

	// Relationships -------------------------------------------

	private Finder finder;
	private Collection<Likes> sentLikes;
	private Collection<Likes> receivedLikes;
	private CreditCard creditCard;
	private Collection<Chirp> sentChirps;
	private Collection<Chirp> receivedChirps;

	@NotNull
	@Valid
	@OneToOne(optional=false,mappedBy="chorbi",cascade=CascadeType.REMOVE)
	public Finder getFinder() {
		return finder;
	}
	public void setFinder(Finder finder) {
		this.finder = finder;
	}
	
	@Valid
	@OneToMany(mappedBy="sender",cascade=CascadeType.REMOVE)
	public Collection<Likes> getSentLikes() {
		return sentLikes;
	}
	public void setSentLikes(Collection<Likes> sentLikes) {
		this.sentLikes = sentLikes;
	}
	
	@Valid
	@OneToMany(mappedBy="receiver",cascade=CascadeType.REMOVE)
	public Collection<Likes> getReceivedLikes() {
		return receivedLikes;
	}
	public void setReceivedLikes(Collection<Likes> receivedLikes) {
		this.receivedLikes = receivedLikes;
	}
	
	@Valid
	@OneToOne(optional=true,mappedBy="chorbi",cascade=CascadeType.REMOVE)
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	@Valid
	@OneToMany(mappedBy="sender",cascade=CascadeType.REMOVE)
	public Collection<Chirp> getSentChirps() {
		return sentChirps;
	}
	public void setSentChirps(Collection<Chirp> sentChirps) {
		this.sentChirps = sentChirps;
	}
	
	@Valid
	@OneToMany(mappedBy="receiver",cascade=CascadeType.REMOVE)
	public Collection<Chirp> getReceivedChirps() {
		return receivedChirps;
	}
	public void setReceivedChirps(Collection<Chirp> receivedChirps) {
		this.receivedChirps = receivedChirps;
	}
}
