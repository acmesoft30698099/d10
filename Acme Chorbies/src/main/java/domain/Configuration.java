package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Configuration extends DomainEntity {
	
	// Constructor ---------------------------------------
	
	public Configuration() {
		super();
	}
	
	// Attributes ----------------------------------------
	
	private Collection<Url> banners;
	private Date timer;
	
	@ElementCollection
	@NotNull
	@Valid
	public Collection<Url> getBanners() {
		return banners;
	}
	public void setBanners(Collection<Url> banners) {
		this.banners = banners;
	}
	
	@DateTimeFormat(pattern="HH:mm:ss")
	@Temporal(TemporalType.TIME)
	public Date getTimer() {
		return timer;
	}
	public void setTimer(Date timer) {
		this.timer = timer;
	}

}
