package services;

import java.util.Calendar;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Configuration;

import utilities.AbstractTest;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ConfigurationServiceTests extends AbstractTest {
	
	// Constructor -----------------------------------
	
	public ConfigurationServiceTests() {
		super();
	}
	
	// Services --------------------------------------
	
	@Autowired
	private ConfigurationService configurationService;
	
	// Tests -----------------------------------------
	
	@Test
	public void findConfigurationTest() {
		Configuration configuration = configurationService.findConfiguration();
		Assert.notNull(configuration);
	}
	
	@Test
	public void changeConfiguration() {
		authenticate("admin");
		Configuration configuration = configurationService.findConfiguration();
		// Para evitar modificar el objeto original, causando errores al tratarse del objeto de cache
		Configuration confNew = new Configuration();
		confNew.setId(configuration.getId());
		confNew.setVersion(configuration.getVersion());
		confNew.setBanners(configuration.getBanners());
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 2);
		calendar.set(Calendar.MINUTE, 30);
		calendar.set(Calendar.SECOND, 5);
		Date timer = calendar.getTime();
		confNew.setTimer(timer);
		Configuration saved = configurationService.save(confNew);
		configurationService.flush();
		saved = configurationService.findConfiguration();
		Assert.notNull(saved);
		calendar.setTime(timer);
		System.out.println(timer.toString());
		Assert.isTrue(calendar.get(Calendar.HOUR_OF_DAY) == 2);
		Assert.isTrue(calendar.get(Calendar.MINUTE) == 30);
		Assert.isTrue(calendar.get(Calendar.SECOND) == 5);
		unauthenticate();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void changeConfigurationIncorrectUser() {
		unauthenticate();
		Configuration configuration = configurationService.findConfiguration();
		// Para evitar modificar el objeto original, causando errores al tratarse del objeto de cache
		Configuration confNew = new Configuration();
		confNew.setId(configuration.getId());
		confNew.setVersion(configuration.getVersion());
		confNew.setBanners(configuration.getBanners());
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 2);
		calendar.set(Calendar.MINUTE, 30);
		calendar.set(Calendar.SECOND, 5);
		Date timer = calendar.getTime();
		confNew.setTimer(timer);
		Configuration saved = configurationService.save(confNew);
		configurationService.flush();
		saved = configurationService.findConfiguration();
		Assert.notNull(saved);
		calendar.setTime(timer);
		System.out.println(timer.toString());
		Assert.isTrue(calendar.get(Calendar.HOUR_OF_DAY) == 2);
		Assert.isTrue(calendar.get(Calendar.MINUTE) == 30);
		Assert.isTrue(calendar.get(Calendar.SECOND) == 5);
	}

}
