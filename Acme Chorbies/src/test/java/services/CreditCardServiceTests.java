package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.CreditCard;

@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CreditCardServiceTests extends AbstractTest {
	
	// Constructor -----------------------------------
	
	public CreditCardServiceTests() {
		super();
	}
	
	// Services --------------------------------------
	
	@Autowired
	private CreditCardService creditCardService;
	
	// Tests -----------------------------------------
	
	@Test
	public void editCreditCard() {
		authenticate("chorbi1");
		CreditCard creditCard = creditCardService.findOneForEdit(13);
		creditCard.setCvv(534);
		CreditCard saved = creditCardService.save(creditCard);
		Assert.isTrue(saved.getCvv().equals(534));
		unauthenticate();
	}
	
	@Test
	public void editValidCard() {
		authenticate("chorbi1");
		CreditCard creditCard = creditCardService.findOneForEdit(13);
		creditCard.setNumber("12345678903");
		creditCard.setBrand("VISA");
		creditCard.setExpirationMonth(1);
		creditCard.setExpirationYear(2018);
		CreditCard saved = creditCardService.save(creditCard);
		Assert.isTrue(saved.getValid());
		unauthenticate();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void editInvalidCard() {
		authenticate("chorbi1");
		CreditCard creditCard = creditCardService.findOneForEdit(13);
		creditCard.setNumber("12345678703");
		creditCard.setBrand("Vsdsd");
		creditCard.setExpirationMonth(8);
		creditCard.setExpirationYear(2010);
		CreditCard saved = creditCardService.save(creditCard);
		Assert.isTrue(saved.getValid());
		unauthenticate();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void editIncorrectUserCreditCard() {
		unauthenticate();
		//Ya que se ejecutará el reconstruct y el reconstruct contiene findOneForEdit
		CreditCard creditCard = creditCardService.findOneForEdit(13);
		creditCard.setCvv(534);
		creditCardService.save(creditCard);
		unauthenticate();
	}
	
}
